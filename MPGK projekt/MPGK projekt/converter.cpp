#include "converter.h"
#include "stdafx.h"

//extern const float PI = atan(1.0)*4;

const float PI = atan(1.0) * 4;  //arcus tangens z 1 = pi/4

float toRadians(float input) {
	return (input * PI / 180);
}

float toDegrees(float input) {
	return (input * 180 / PI);
}