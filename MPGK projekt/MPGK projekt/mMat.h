#pragma once
#include "converter.h"
#include "mVec.h"
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <tchar.h>

//#include "mVec.h"
using namespace std;

class mMat
{
public:
	// zestaw konstruktorów
	mMat(void);										// domyslny konstruktor bez parametrów
	mMat(float);									// domyslny konstruktor przyjmujacy liczbe type float uzupelniajacy macierz kwadratowa ta liczba
	mMat(float**);									// konstruktor przyjmujacy tablice float* tworzacy macierz uzupelniona zgodnie z tablica
	~mMat(void);									// domyslny destruktor

													// przeciążone operatory 
	const mMat& operator=(const mMat&);				// przeciażony operator przypisania operujący na wskaźnikach (bez tworzenia nowego obiektu)
	const mMat operator+(const mMat&);				// przeciążony operator dodawania tworzący nowy obiekt wynikowy
	const mMat& operator+=(const mMat&);			// przeciążony operator += operujący na wskaźnikach (bez tworzenia nowego obiektu)
	const mMat operator-(const mMat&);				// przeciążony operator odejmowania tworzący nowy obiekt wynikowy
	const mMat& operator-=(const mMat&);			// przeciążony operator -= operujący na wskaźnikach (bez tworzenia nowego obiektu)
	const mMat operator*(float);					// przeciążony operator mnożenia macierzy przez liczbę
	const mMat& operator*=(float);					// przeciążony operator *= przez liczbę
	const mMat operator*(const mMat&);				// przeciążony operator mnożenia macierzy przez siebie
	const mMat& operator*=(const mMat&);			// przeciążony operator *= przez macierz
	const mMat operator*(const mVec&);				// przeciążony operator mnożenia macierzy przez wektor

													// funkcje
	mMat setter(mMat, int, int, float);			// deklaracja funkcji setter - przyjmuje obiekt klasy macierz oraz odpowiednio: kolumnę, wierz i wartość
	float getter(mMat, int, int);				// deklaracja funkcji getter - przyjmuje obiekt klasy macierz oraz odpowiednio: kolumnę i wiesz
	mMat unitMat(int);							// deklaracja funkcji tworzącej macierz jednostkową
	mMat transMat(mMat);						// deklaracja funkcji wykonującej transponowanie macierzy
												//friend ostream& operator<< (ostream&, const mMat&);
	mMat scale2(float);
	mMat scale3(float);
	mMat scale2(float, float);
	mMat scale3(float, float, float);
	mMat scale2(float*);
	mMat scale3(float*);
	mMat rot2(float);
	mMat rot3(float, float, float);
	mMat rot3(int, float);
	mMat trans2(float, float);
	mMat trans2(float*);
	mMat trans3(float*);
	mMat trans3(float, float, float);
	mMat oper(mMat, mMat*);
	mMat perspective(float, float, float, float, float);

private:
	int rows, cols;
	float** params;



};

