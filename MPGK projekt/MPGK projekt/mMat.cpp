#include "stdafx.h"
#include "mMat.h"
#include "mVec.h"

mMat::mMat() {
	rows = 1;
	cols = 1;
	params[0][0] = 0;
}

mMat::mMat(float k) {
	rows = k;
	cols = k;
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			params[i][j] = 0;
		}
	}
}

mMat::mMat(float** input) {
	//params = input;
	rows = sizeof(input) / sizeof(input[0]);
	cols = sizeof(input[0]) / sizeof(int);
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			params[i][j] = input[i][j];
		}
	}
}

mMat::~mMat() {
	for (int i = 0; i < rows; i++) {
		delete[] params[i];
	}
	delete[] params;
}

mMat mMat::transMat(mMat input) {
	mMat output;
	output.rows = input.cols;
	output.cols = input.rows;
	for (int i = 0; i < input.rows; i++) {
		for (int j = 0; j < input.cols; j++) {
			output.params[j][i] = input.params[i][j];
		}
	}
	return output;
}

mMat mMat::unitMat(int size) {
	mMat output(size);
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			if (i == j) {
				output.params[i][j] = 1;
			}
			else {
				output.params[i][j] = 0;
			}
		}
	}
	return output;
}


mMat mMat::setter(mMat input, int row, int col, float x) {
	mMat output;
	output.rows = input.rows;
	output.cols = input.cols;
	output = input;
	output.params[row][col] = x;
	return output;
}

float mMat::getter(mMat input, int row, int col) {
	return input.params[row][col];
}

const mMat& mMat::operator=(const mMat& temp) {
	if (this == &temp) {
		return *this;
	}

	if (rows != temp.rows || cols != temp.cols) {
		for (int i = 0; i < rows; i++) {
			delete[] params[i];
		}
		delete[] params;

		rows = temp.rows;
		cols = temp.cols;
	}

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			params[i][j] = temp.params[i][j];
		}
	}
	return *this;
}


const mMat mMat::operator+(const mMat& temp) {
	if (rows != temp.rows || cols != temp.cols) {
		cout << "Cos sie chyba wymiary macierzy pomieszaly...\n";
		return temp;
	}

	mMat rez;
	rez.rows = rows;
	rez.cols = cols;

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			rez.params[i][j] = params[i][j] + temp.params[i][j];
		}
	}

	return rez;
}



const mMat& mMat::operator+=(const mMat& temp) {
	if (rows != temp.rows || cols != temp.cols) {
		cout << "Cos sie chyba wymiary macierzy pomieszaly...\n";
		return temp;
	}

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			params[i][j] += temp.params[i][j];
		}
	}
	return *this;
}


const mMat mMat::operator-(const mMat& temp) {
	if (rows != temp.rows || cols != temp.cols) {
		cout << "Cos sie chyba wymiary macierzy pomieszaly...\n";
		return temp;
	}

	mMat rez;
	rez.rows = rows;
	rez.cols = cols;

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			rez.params[i][j] = params[i][j] - temp.params[i][j];
		}
	}

	return rez;
}

const mMat& mMat::operator-=(const mMat& temp) {
	if (rows != temp.rows || cols != temp.cols) {
		cout << "Cos sie chyba wymiary macierzy pomieszaly...\n";
		return temp;
	}

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			params[i][j] -= temp.params[i][j];
		}
	}
	return *this;
}


const mMat mMat::operator*(const mMat& temp) {
	mMat rez;
	rez.rows = rows;
	rez.cols = cols;

	if (rows != temp.cols) {
		cout << "Cos sie pomieszalo i wymiary mnozonych macierzy sie nie zgadzaja...\n";
		return temp;
	}

	for (int i = 0; i < rez.rows; i++) {
		for (int j = 0; j < rez.cols; j++) {
			for (int k = 0; k < cols; k++) {
				rez.params[i][j] += (params[i][k] * temp.params[k][j]);
			}
		}
	}

	return rez;
}

const mMat& mMat::operator*=(const mMat& temp) {
	if (rows != temp.cols) {
		cout << "Cos sie pomieszalo i wymiary mnozonych macierzy sie nie zgadzaja...\n";
		return temp;
	}

	mMat rez;
	rez.rows = rows;
	rez.cols = temp.cols;

	for (int i = 0; i < rez.rows; i++) {
		for (int j = 0; j < rez.cols; j++) {
			for (int k = 0; k < cols; k++) {
				rez.params[i][j] += (params[i][k] * temp.params[k][j]);
			}
		}
	}
	return (*this = rez);
}


const mMat mMat::operator*(float x) {
	mMat temp;
	temp.rows = rows;
	temp.cols = cols;
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			temp.params[i][j] = params[i][j] * x;
		}
	}

	return temp;
}

const mMat& mMat::operator*=(float x) {
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			params[i][j] *= x;
		}
	}

	return *this;
}

mMat mMat::scale2(float k) {
	mMat rez;
	rez.rows = 2;
	rez.cols = 2;
	for (int i = 0; i < rez.rows; i++) {
		for (int j = 0; j < rez.cols; j++) {
			if (i == j) {
				rez.params[i][j] = k;
			}
			else {
				rez.params[i][j] = 0;
			}
		}
	}

	return rez;
}

mMat mMat::scale3(float k) {
	mMat rez;
	rez.rows = 3;
	rez.cols = 3;
	for (int i = 0; i < rez.rows; i++) {
		for (int j = 0; j < rez.cols; j++) {
			if (i == j) {
				rez.params[i][j] = k;
			}
			else {
				rez.params[i][j] = 0;
			}
		}
	}

	return rez;
}

mMat mMat::scale2(float k, float l) {
	float temp[2] = { k, l };
	mMat rez;
	rez.rows = 2;
	rez.cols = 2;
	for (int i = 0; i < rez.rows; i++) {
		for (int j = 0; j < rez.cols; j++) {
			if (i == j) {
				rez.params[i][j] = temp[i];
			}
			else {
				rez.params[i][j] = 0;
			}
		}
	}

	return rez;
}

mMat mMat::scale3(float k, float l, float m) {
	float temp[3] = { k, l, m };
	mMat rez;
	rez.rows = 3;
	rez.cols = 3;
	for (int i = 0; i < rez.rows; i++) {
		for (int j = 0; j < rez.cols; j++) {
			if (i == j) {
				rez.params[i][j] = temp[i];
			}
			else {
				rez.params[i][j] = 0;
			}
		}
	}

	return rez;
}

mMat mMat::scale2(float* temp) {
	mMat rez;
	rez.rows = 2;
	rez.cols = 2;
	for (int i = 0; i < rez.rows; i++) {
		for (int j = 0; j < rez.cols; j++) {
			if (i == j) {
				rez.params[i][j] = temp[i];
			}
			else {
				rez.params[i][j] = 0;
			}
		}
	}

	return rez;
}

mMat mMat::scale3(float* temp) {
	mMat rez;
	rez.rows = 3;
	rez.cols = 3;
	for (int i = 0; i < rez.rows; i++) {
		for (int j = 0; j < rez.cols; j++) {
			if (i == j) {
				rez.params[i][j] = temp[i];
			}
			else {
				rez.params[i][j] = 0;
			}
		}
	}

	return rez;
}

mMat mMat::rot2(float angle) {
	mMat temp;
	temp.rows = 2;
	temp.cols = 2;
	temp.params[0][0] = cos(toRadians(angle));
	temp.params[0][1] = sin(toRadians(angle));
	temp.params[1][0] = -sin(toRadians(angle));
	temp.params[1][1] = cos(toRadians(angle));

	return temp;
}

mMat mMat::rot3(float alfa, float beta, float gamma) {
	mMat rx, ry, rz;
	rx.rows = 3;
	rx.cols = 3;
	ry.rows = 3;
	ry.cols = 3;
	rz.rows = 3;
	rz.cols = 3;

	// zbudowanie macierzy Rx obracajacej wokol osi X
	rx.params[0][0] = 1;
	rx.params[0][1] = 0;
	rx.params[0][2] = 0;
	rx.params[1][0] = 0;
	rx.params[1][1] = cos(toRadians(alfa));
	rx.params[1][2] = -sin(toRadians(alfa));
	rx.params[2][0] = 0;
	rx.params[2][1] = sin(toRadians(alfa));
	rx.params[2][2] = cos(toRadians(alfa));

	// zbudowanie macierzy Ry obracajacej wokol osi Y
	ry.params[0][0] = cos(toRadians(beta));
	ry.params[0][1] = 0;
	ry.params[0][2] = sin(toRadians(beta));
	ry.params[1][0] = 0;
	ry.params[1][1] = 1;
	ry.params[1][2] = 0;
	ry.params[2][0] = -sin(toRadians(beta));
	ry.params[2][1] = 0;
	ry.params[2][2] = cos(toRadians(beta));

	// zbudowanie macierzy Rz obracajacej wokol osi Z
	rz.params[0][0] = cos(toRadians(gamma));
	rz.params[0][1] = -sin(toRadians(gamma));
	rz.params[0][2] = 0;
	rz.params[1][0] = sin(toRadians(gamma));
	rz.params[1][1] = cos(toRadians(gamma));
	rz.params[1][2] = 0;
	rz.params[2][0] = 0;
	rz.params[2][1] = 0;
	rz.params[2][2] = 1;

	// wynikowa macierz obrotu to iloczyn macierzy w kolejnosci Rz*Ry*Rz
	mMat rez;
	rez.rows = 3;
	rez.cols = 3;
	rez = rz * ry;
	rez *= rx;

	return rez;
}

mMat mMat::rot3(int x, float angle) {
	mMat rx, ry, rz;
	rx.rows = 3;
	rx.cols = 3;
	ry.rows = 3;
	ry.cols = 3;
	rz.rows = 3;
	rz.cols = 3;

	// zbudowanie macierzy Rx obracajacej wokol osi X
	rx.params[0][0] = 1;
	rx.params[0][1] = 0;
	rx.params[0][2] = 0;
	rx.params[1][0] = 0;
	rx.params[1][1] = cos(toRadians(angle));
	rx.params[1][2] = -sin(toRadians(angle));
	rx.params[2][0] = 0;
	rx.params[2][1] = sin(toRadians(angle));
	rx.params[2][2] = cos(toRadians(angle));

	// zbudowanie macierzy Ry obracajacej wokol osi Y
	ry.params[0][0] = cos(toRadians(angle));
	ry.params[0][1] = 0;
	ry.params[0][2] = sin(toRadians(angle));
	ry.params[1][0] = 0;
	ry.params[1][1] = 1;
	ry.params[1][2] = 0;
	ry.params[2][0] = -sin(toRadians(angle));
	ry.params[2][1] = 0;
	ry.params[2][2] = cos(toRadians(angle));

	// zbudowanie macierzy Rz obracajacej wokol osi Z
	rz.params[0][0] = cos(toRadians(angle));
	rz.params[0][1] = -sin(toRadians(angle));
	rz.params[0][2] = 0;
	rz.params[1][0] = sin(toRadians(angle));
	rz.params[1][1] = cos(toRadians(angle));
	rz.params[1][2] = 0;
	rz.params[2][0] = 0;
	rz.params[2][1] = 0;
	rz.params[2][2] = 1;

	if (x == 1) {
		return rx;
	}
	else if (x == 2) {
		return ry;
	}
	else if (x == 3) {
		return rz;
	}
	else {
		cout << "Cos nie taki argument zostal wprowadzony, ma byc 1, 2 lub 3...\n";
		return errno;
	}
}

mMat mMat::trans2(float* tmp) {
	if (sizeof(tmp) != 2) {
		cout << "Cos zly ten argument, tablica powinna miec 2 elementy...\n";
		return errno;
	}

	mMat t;
	t.params[0][0] = 1;
	t.params[0][1] = 0;
	t.params[0][2] = tmp[0];
	t.params[1][0] = 0;
	t.params[1][1] = 1;
	t.params[1][2] = tmp[1];
	t.params[2][0] = 0;
	t.params[2][1] = 0;
	t.params[2][2] = 1;

	return t;
}

mMat mMat::trans2(float x, float y) {

	mMat t;
	t.params[0][0] = 1;
	t.params[0][1] = 0;
	t.params[0][2] = x;
	t.params[1][0] = 0;
	t.params[1][1] = 1;
	t.params[1][2] = y;
	t.params[2][0] = 0;
	t.params[2][1] = 0;
	t.params[2][2] = 1;

	return t;
}


mMat mMat::trans3(float* tmp) {
	if (sizeof(tmp) != 3) {
		cout << "Cos sie argument pomylil, tablica powinna miec 3 elementy...\n";
		return errno;
	}

	mMat t;
	t.params[0][0] = 1;
	t.params[0][1] = 0;
	t.params[0][2] = 0;
	t.params[0][3] = tmp[0];
	t.params[1][0] = 0;
	t.params[1][1] = 1;
	t.params[1][2] = 0;
	t.params[1][3] = tmp[1];
	t.params[2][0] = 0;
	t.params[2][1] = 0;
	t.params[2][2] = 1;
	t.params[2][3] = tmp[2];
	t.params[3][0] = 0;
	t.params[3][1] = 0;
	t.params[3][2] = 0;
	t.params[3][3] = 1;

	return t;
}

mMat mMat::trans3(float x, float y, float z) {
	mMat t;
	t.params[0][0] = 1;
	t.params[0][1] = 0;
	t.params[0][2] = 0;
	t.params[0][3] = x;
	t.params[1][0] = 0;
	t.params[1][1] = 1;
	t.params[1][2] = 0;
	t.params[1][3] = y;
	t.params[2][0] = 0;
	t.params[2][1] = 0;
	t.params[2][2] = 1;
	t.params[2][3] = z;
	t.params[3][0] = 0;
	t.params[3][1] = 0;
	t.params[3][2] = 0;
	t.params[3][3] = 1;

	return t;
}

mMat mMat::oper(mMat input, mMat* temp) {
	for (int i = 0; i < sizeof(temp); i++) {
		input *= temp[i];
	}
	return input;
}

mMat mMat::perspective(float angle, float h, float w, float nearz, float farz) {
	mMat t;
	float a = w / h;
	float f = 1 / tan(toRadians(angle));
	t.params[0][0] = f / a;
	t.params[0][1] = 0;
	t.params[0][2] = 0;
	t.params[0][3] = 0;
	t.params[1][0] = 0;
	t.params[1][1] = f;
	t.params[1][2] = 0;
	t.params[1][3] = 0;
	t.params[2][0] = 0;
	t.params[2][1] = 0;
	t.params[2][2] = (farz + nearz) / (nearz - farz);
	t.params[2][3] = (2 * farz * nearz) / (nearz - farz);
	t.params[3][0] = 0;
	t.params[3][1] = 0;
	t.params[3][2] = -1;
	t.params[3][3] = 0;

	return t;
	// zrodlo: https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluPerspective.xml
}
