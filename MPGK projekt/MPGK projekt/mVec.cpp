#include "stdafx.h"
#include "mVec.h"


mVec::mVec() {
	size = 0;
}

mVec::mVec(int s) {
	if (s < 0 && s > 4) {
		cout << "Cos chyba sie wymiary pomylily..." << "\n";
	}
	else {
		size = s;
		for (int i = 0; i < s; i++) {
			params[i] = 0;
		}
	}
}

mVec::mVec(float x, float y, float z) {
	params[0] = x;
	params[1] = y;
	params[2] = z;
	params[3] = 0;
	size = sizeof(params);
}

mVec::mVec(float x, float y, float z, float w) {
	params[0] = x;
	params[1] = y;
	params[2] = z;
	params[3] = w;
	size = sizeof(params);
}

mVec::mVec(float elems[]) {
	for (int i = 0; i < sizeof(elems); i++) {
		params[i] = elems[i];
	}
	size = sizeof(elems);
}

mVec::~mVec() {
	cout << "Wlasnie zniszczyles obiekt klasy mVec.\n";
	delete[] params;
}

// przeciazony operator = operuje na wskaznikach bez koniecznosci tworzenia nowego obiektu w pamieci
const mVec& mVec::operator=(const mVec& tmp) {
	float* k = new float[tmp.size];
	for (int i = 0; i < size; i++) {
		k[i] = tmp.params[i];
	}
	delete[] params;
	this->params = k;
	this->size = tmp.size;
	return *this;
}

// przeciazony operator + operuje na nowo utworzonym obiekcie (dodatkowa alokacja pamieci) - utworzono bez wskaznika do testu
const mVec mVec::operator+(const mVec& tmp) {
	if (this->size != tmp.size) {
		cout << "Cos sie wymiary w dodawanych wektorach pomylily...\n";
		return NULL;
	}
	mVec vSum = size;
	for (int i = 0; i < this->size; i++) {
		vSum.params[i] = params[i] + tmp.params[i];
	}
	return vSum;
}

// przeciazony operator += operuje na wskaznikach bez koniecznosci tworzenia nowego obiektu
const mVec& mVec::operator+=(const mVec& tmp) {
	if (size != tmp.size) {
		cout << "Cos sie wymiary pomieszaly...\n";
		return NULL;
	}
	for (int i = 0; i < size; i++) {
		params[i] += tmp.params[i];
	}
	/*
	float* k = new float[size + tmp.size];
	for (int i = 0; i < size; i++) {
	k[i] = params[i];
	}
	for (int i = size, ctr = 0; i < size + tmp.size; i++, ctr++) {
	k[i] = tmp.params[ctr];
	}
	delete[] params;
	this->params = k;
	this->size += tmp.size;*/
	return *this;
}

// przeciazony operator - wymaga utworzenia nowego obiektu bedacego wynikiem (dodatkowa alokacja pamieci)
const mVec mVec::operator-(const mVec& tmp) {
	if (this->size != tmp.size) {
		cout << "Cos sie wymiary w odejmowanych wektorach pomylily...\n";
		return NULL;
	}
	mVec vRoz = size;
	for (int i = 0; i < this->size; i++) {
		vRoz.params[i] = params[i] - tmp.params[i];
	}
	return vRoz;
}

// przeciazony operator -= operuje na wskaznikach bez koniecznosci tworzenia nowego obiektu klasy mVec
const mVec& mVec::operator-=(const mVec& tmp) {
	if (size != tmp.size) {
		cout << "Cos sie wymiary pomieszaly...\n";
		return NULL;
	}
	for (int i = 0; i < size; i++) {
		params[i] -= tmp.params[i];
	}
	return *this;
}

// przeciazony operator * z tworzeniem nowego obiektu bedacego wynikiem dzialania
const mVec mVec::operator*(int a) {
	mVec vIlo = size;
	for (int i = 0; i < size; i++) {
		vIlo.params[i] = params[i] * a;
	}
	return vIlo;
}

// przeciazony operaotr *= nie wymaga tworzenia nowego obiektu i operuje na wskaznikach
const mVec& mVec::operator*=(int a) {
	for (int i = 0; i < size; i++) {
		params[i] *= a;
	}
	return *this;
}

// przeciazenie operatora standardowego wyjscia
ostream& operator<<(ostream &out, const mVec &tmp) {
	out << "Wektor ma rozmiar " << tmp.size << ".\n" << "A jego parametry to: [" << tmp.params[0] << " ," << tmp.params[1] << " , " << tmp.params[2] << " , " << tmp.params[3] << "].\n";
	return out;
}


// funkcja setter, przyjmuje obiekt klasy wektor oraz tablice typu float wraz z nowymi wartosciami wspolrzednych wektora
// posiada weryfikator podawanego rozmiaru, wazne aby tablica nie byla krotsza od wektora
mVec mVec::setter(mVec input, float* tmp) {
	if (input.size != sizeof(tmp)) {
		cout << "Cos sie wymiary pomieszaly... sprobuj ponownie.\n";
		return NULL;
	}
	for (int i = 0; i < size; i++) {
		input.params[i] = tmp[i];
	}
	return input;
}


// funkcja getter, przyjmuje jako argumenty wektor wejsciowy oraz liczbe typu int, ktora wskazuje wspolrzedna wektora, ktora nalezy zwrocic
// posiada weryfikator podawanych argumentow - wazne, aby int i nie byla wieksza od rozmiaru wektora
float mVec::getter(mVec input, int i) {
	if (input.size < i) {
		cout << "Troche zbyt duzy ten parametr...\n";
	}
	else
		return input.params[i];
}


// iloczyn skalarny opisany za pomoca uniwersalnego wzoru matematycznego wykorzystujacy operator += pomiedzy kolejnymi iteracjami
float mVec::mScalar(mVec a, mVec b) {
	if (a.size != b.size) {
		cout << "Cos sie wymiary pomieszaly... sprobuj ponownie.\n";
		return NULL;
	}
	float result = 0;
	for (int i = 0; i < a.size; i++) {
		result += (a.params[i] * b.params[i]);
	}
	return result;
}

// iloczyn wektorowy opisany za pomoca ogolnej formuly matematycznej, jest liczony jedynie dla wektorow posiadajacych 3 wspolrzedne
mVec mVec::mVector(mVec a, mVec b) {
	mVec result = a.size;
	if (a.size == b.size) {
		result.params[0] = a.params[1] * b.params[2] - b.params[1] * a.params[2];
		result.params[1] = b.params[0] * a.params[2] - a.params[0] * b.params[2];
		result.params[2] = a.params[0] * b.params[1] - b.params[0] * a.params[1];
	}
	return result;
}