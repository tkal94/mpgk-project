#pragma once
#include "converter.h"
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <tchar.h>
using namespace std;

class mVec
{
public:
	// zestaw konstruktorów
	mVec(void);								// pusty konstruktor bez parametrów
	mVec(int);								// konstruktor z 1 parametrem (rozmiar wektora)
	mVec(float, float, float, float);		// konstruktor z 4 parametrami (wspolrzedne)
	mVec(float, float, float);				// konstruktor z 3 parametrami (wspolrzedne w 3 wymiaramch)
	mVec(float[]);							// konstruktor przyjmujacy tablice float
	~mVec(void);							// domyslny destruktor


											// przeciążone operatory
	const mVec& operator=(const mVec&);
	const mVec operator+(const mVec&);
	const mVec& operator+=(const mVec&);
	const mVec operator-(const mVec&);
	const mVec& operator-=(const mVec&);
	const mVec operator*(int);
	const mVec& operator*=(int);
	//const mVec& operator<<(const mVec&); zrobione za pomoca funkcji friend


	// funkcje
	mVec setter(mVec, float[]);				// deklaracja funkcji setter - ma ustawic parametry wektora z argumentu zgodnie z tablica float
	float getter(mVec, int);				// deklaracja funkcji getter - ma wyciagac wskazany parametr wektora, numer porzadkowy (x,y,z,w) podawany w drugim argumencie
	mVec mNormalize(mVec);					// deklaracja funkcji normalizujacej
	float mScalar(mVec, mVec);				// deklaracja iloczynu skalarnego
	mVec mVector(mVec, mVec);				// deklaracja iloczynu wektorowego
	friend ostream& operator<< (ostream&, const mVec&);

private:
	float* params;							// w tablicy params przechowywane sa wspolrzedne wektora (x,y,z,w)
	int size;								// dodatkowy atrybut rozmiaru - przewaznie generowany przez sizeof(params)
};

